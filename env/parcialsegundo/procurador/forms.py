from django import forms
from .models import Procurador

class ProcuradorForm(forms.ModelForm):
    class Meta:
        model = Procurador
        fields = '__all__'
