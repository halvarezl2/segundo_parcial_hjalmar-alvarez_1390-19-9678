# En el archivo views.py de la aplicación "procurador"
from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Procurador


def listar_procuradores(request):
    procuradores = Procurador.objects.all()
    return render(request, 'listar_procuradores.html', {'procuradores': procuradores})


def crear_procurador(request):
    if request.method == 'POST':
        form = Procurador(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Procurador creado con éxito.')
            return redirect('listar_procuradores')
    else:
        form = Procurador()
    return render(request, 'crear_procurador.html', {'form': form})


def modificar_procurador(request, pk):
    procurador = Procurador.objects.get(pk=pk)
    if request.method == 'POST':
        form = Procurador(request.POST, instance=procurador)
        if form.is_valid():
            form.save()
            messages.success(request, 'Procurador modificado con éxito.')
            return redirect('listar_procuradores')
    else:
        form = Procurador(instance=procurador)
    return render(request, 'modificar_procurador.html', {'form': form})


def eliminar_procurador(request, pk):
    procurador = Procurador.objects.get(pk=pk)
    if request.method == 'POST':
        procurador.delete()
        messages.success(request, 'Procurador eliminado con éxito.')
        return redirect('listar_procuradores')
    return render(request, 'eliminar_procurador.html', {'procurador': procurador})
