
from django.db import models
from django.contrib.auth.models import User

class Procurador(models.Model):
    dni = models.CharField(max_length=9)
    casos_ganados = models.PositiveIntegerField()
    nombre = models.CharField(max_length=20)
    apellido = models.CharField(max_length=20)
    numero_colegiado = models.PositiveIntegerField()

    def __str__(self):
        return f"{self.nombre} {self.apellido}"
