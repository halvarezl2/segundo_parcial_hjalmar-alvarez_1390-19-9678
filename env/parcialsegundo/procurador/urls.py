# En el archivo urls.py de la aplicación "procurador"
from django.urls import path
from . import views

urlpatterns = [
    path('', views.listar_procuradores, name='listar_procuradores'),
    path('crear/', views.crear_procurador, name='crear_procurador'),
    path('modificar/<int:pk>/', views.modificar_procurador, name='modificar_procurador'),
    path('eliminar/<int:pk>/', views.eliminar_procurador, name='eliminar_procurador'),
]
